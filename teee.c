#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>

#ifndef MAX_BUF
#define MAX_BUF 1024
#endif

void usage(void) {
	fprintf(stderr, "Usage: teee [-a] [-s] <source_file> [-d ] <destination_file>\n");
}

int main(int argc, char **argv) {

	int opt, oflags = O_WRONLY | O_TRUNC | O_CREAT, sourcefd, targetfd, bread = 0;
	mode_t operms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWOTH | S_IROTH;
	char *buf = malloc(MAX_BUF);

	if(argc < 4 || argc >= 7){
		usage();
		exit(1);
	}

	while((opt = getopt(argc, argv, "as:d:")) != -1) {
		switch(opt) {
			case 's':
				if((sourcefd = open((optarg), O_RDONLY)) == -1) {
					fprintf(stderr, "Error opening file %s",optarg);
					perror(" ");
					exit(2);
				}
				break;
			case 'd':
				if((targetfd = open((optarg), oflags, operms)) == -1) {
					fprintf(stderr, "Error opening file %s", optarg);
					perror(" ");
					exit(3);
				}
				break;
			case 'a':
				oflags = O_APPEND | O_WRONLY;
				break;
			default:
				usage();
				exit(1);
		}
	}

	while((bread = read(sourcefd, buf, MAX_BUF)) > 0) {
		if((write(targetfd, buf, bread)) != bread) {
			perror("Error writing to target file ");
			exit(5);
		}
		if((write(1, buf, bread)) != bread) {
			perror("Error writing to stdout ");
			exit(6);
		}
	}

	exit(0);
}
