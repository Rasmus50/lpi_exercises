#define _LARGEFILE64_SOURCE
#include <sys/stat.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int fd;
	off64_t offset;

	if (argc != 3 || strcmp(argv[1], "--help") == 0) {
		fprintf(stderr,"%s <pathname> <offset>\n", argv[0]);
		exit(1);
	}

	if(fd = open64(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR) == -1) {
		perror("open ");
		exit(2);
	}

	offset = atoll(argv[2]);
	if(lseek64(fd, offset, SEEK_SET) == -1) {
		perror("lseek ");
		exit(3);
	}

	if(write(fd, "test", 4) == -1) {
		perror("write ");
		exit(4);
	}

	exit(0);
}
	
