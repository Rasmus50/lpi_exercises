#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

int main(void) {

	DIR *dirp;
	int comfd;
	struct dirent *dp;
	struct stat *infobuf = malloc(500);
	const char *proc = "/proc/";
	char dirpath[256];
	int info;
	char *buf; 
	buf = malloc(256);
	if(!buf)
	{
		perror("!E: malloc");
		exit(1);
	}

	dirp = opendir(proc); 
	if(!dirp) {
		perror("!E: opendir");
		exit(2);
	}

	for (;;) {
		dp = readdir(dirp);
		if(!dp)
			break;

		// assemble "/proc/name_of_process"
		strcpy(dirpath, proc);
		strcat(dirpath, dp->d_name);

		//load file attributes into buffer structure
		if(stat(dirpath, infobuf) == -1) {
			perror("!E: stat");
			exit(3);
		}

		// check the uid of a file by casting st_uid
		if((long)infobuf->st_uid != getuid())
			continue;
		printf("%s -- ", dp->d_name);
		
		// add "/cmdline" onto the end of the path
		strcat(dirpath, "/cmdline");

		// open the cmdline file for a given process
		comfd = open(dirpath, O_RDONLY);
		if(comfd == -1) {
			perror("!E: open");
			fprintf(stderr, "%s\n", dirpath);
			continue;
		}

		//read the command line arguments of the file into buf
		if(read(comfd, buf, 255) == -1) {
			perror("!E: read");
			continue;
		}
		printf("%s\n", buf);

		memset(dirpath, '\0', sizeof(dirpath));
	}

	free(buf);

	// close the pointer to directory
	if(closedir(dirp) == -1) {
		perror("!E: closedir");
		exit(5);
	}

	return 0;
}
