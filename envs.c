#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char **environ;

int setenvv(const char *name, const char *value, int overwrite);
int unsetenvv(const char *name);

int setenvv(const char *name, const char *value, int overwrite) {

	int i = 0;
	char *new = calloc(256, 1);
	
	if(name == NULL || name[0] == '\0') {
		fprintf(stderr, "setenvv: Environment variable name argument invalid\n");
		return -1;
	}
	
	// checks to see if the environment variable exists
	for(i; environ[i] != NULL; i++) {
		if(strncmp(environ[i], name, strlen(name)) == 0) {
			// backs out of overwriting if variable is set to 0
			if(overwrite == 0) {
				return 0;
			}
			//break out of the loop and overwrite it
			break;
		}
	}

	
	sprintf(new, "%s=%s", name, value);
	putenv(new);

	return 0;
}

int unsetenvv(const char *name) {

	for(int i = 0; environ[i] != NULL; i++) {
		if(strncmp(environ[i], name, strlen(name)) == 0) {
			environ[i] == NULL;
		}
	}

	return 0;
}

int main(int argc, char **argv) {

	setenvv(argv[1], argv[2], 1);

	if(argc != 3)
		exit(20);

	printf("%s\n", getenv(argv[1]));

	unsetenvv(argv[1]);

	printf("%s\n", getenv(argv[1]));

	return 0;
}
