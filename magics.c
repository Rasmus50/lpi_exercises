#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/reboot.h>


int main(int argc, char **argv) {

	printf("MAGIC1 = 0xfee1dead\n");
	printf("MAGIC2 = 672274793 in hex = 0x%x\n", 672274793);
	printf("MAGIC2A = 85072278 in hex = 0x%x\n", 85072278);
	printf("MAGIC2B = 369367448 in hex = 0x%x\n", 369367448);
	printf("MAGIC2C = 537993216 in hex = 0x%x\n", 537993216);

	printf("terminate process to stop reboot - 5 seconds\n");
	sleep(5);

	reboot(0x1234567);

}
