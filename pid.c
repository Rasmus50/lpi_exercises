#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int main(void) { 
	pid_t pid;
	
	pid = getpid();
	printf("PID is %ld\n", (long) pid);
	puts("--------------------------------------");
	system("ps aux | grep -w pid");

}
