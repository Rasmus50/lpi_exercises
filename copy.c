#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#ifndef MAX_BUF
#define MAX_BUF 1024
#endif

void usage(char *prog) {

	fprintf(stderr, "Usage: %s <source> <target>\n", prog);
	exit(1);

}

int main(int argc, char **argv) {

	int oflags = O_WRONLY | O_TRUNC | O_CREAT, sourcefd, targetfd, bread = 0;
	mode_t operms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
	char *buf = malloc(MAX_BUF);

	if(argc != 3) {
		usage(argv[0]);
	}

	if((sourcefd = open(argv[1], O_RDONLY)) == -1) {
		fprintf(stderr, "Error opening file %s", argv[1]);
		perror(" ");
		exit(2);
	}
	if((targetfd = open(argv[2], oflags, operms)) == -1) {
		fprintf(stderr, "Error opening file %s", argv[2]);
		perror(" ");
		exit(3);
	}

	while((bread = read(sourcefd, buf, MAX_BUF)) > 0) {
		if((write(targetfd, buf, bread)) != bread) {
			fprintf(stderr, "Error writing to file %s", argv[2]);
			perror(" ");
			exit(4);
		}
	}

	exit(0);

}
